using Test
using Flux, CUDA
include("../src/type_aliases.jl")
include("../src/data_utils.jl")
include("../src/models.jl")
include("../src/train_model.jl")

@testset "test losses" begin
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=1.0) .== 0.)
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=0.5) .>= 0.)
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=1.5) .>= 0.)
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=1.0*ones(5)) .== 0.)
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=0.5*ones(5)) .>= 0.)
    @test all(gaussian_kl(zeros(5, 7), zeros(5, 7); λ=1.5*ones(5)) .>= 0.)
  end

@testset "test encoders" for encoder in ["BasicEncoder", "ResidualEncoder", "SAMEncoder"]
    model_device = cpu
    data_for_vis = (1:32, 1:32) |> transform |> first |> first |> model_device
    model = make_vae(size(data_for_vis)[1], 10;
                encoder=encoder, decoder="BasicDecoder",
                sc=1
            ) |> model_device
    model(data_for_vis);
  end

@testset "test decoders" for decoder in ["BasicDecoder", "ResidualDecoder", "StyleGANDecoder"]
    model_device = cpu
    data_for_vis = (1:32, 1:32) |> transform |> first |> first |> model_device
    model = make_vae(size(data_for_vis)[1], 10;
                encoder="BasicEncoder", decoder=decoder,
                sc=1
            ) |> model_device
    model(data_for_vis);
  end
