using ArgParse
using disentangled_vaes

args = parse_args(make_training_args_parser())
enc_device, data_for_vis = run_training(args)
