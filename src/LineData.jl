export LineData

module LineData
    using Random
    export traintensor, testtensor, trainlabels, testlabels, traindata, testdata, draw!

    RAND_SEED_TRAIN = 42
    RAND_SEED_TEST = 43

    dist_point_to_line((x1, y1)::Tuple, (x2, y2)::Tuple, (x0, y0)::Tuple) = 
             abs((x2-x1)*(y1-y0) - (x1-x0)*(y2-y1)) / sqrt((x2-x1)^2 + (y2-y1)^2)

    X = [(x, y) for x in LinRange(0, 1, 28+2)[2:end-1], y in LinRange(0, 1, 28+2)[2:end-1]];
    draw!(img::Array{Float32, 3}, Δy::Float32, α::Float32, c::Vector{Float32}) = begin
      p1 = (0.5, 0.5+Δy)
      p2 = (0.5+cos(α), 0.5+Δy+sin(α))
      f(t) = 1 ./ exp.(800*dist_point_to_line(p1, p2, t)^2)
      Z = f.(X)

      img .+= reshape(Z, size(Z)..., 1) .* reshape(c, 1, 1, :)
      clamp!(img, 0., 1.)
    end

    # We draw on the first image using labels 1:6,
    # then replace the kth label (k \in 1..3) with the numbers stored in label[7:8].
    draw_labels_on_image_two_ways!(img, labels, k) = begin
      img2 = copy(img)
      labels2 = copy(labels)

      draw!(img, labels[1], labels[4], Float32[1., 0., 0.])
      draw!(img, labels[2], labels[5], Float32[0., 1., 0.])
      draw!(img, labels[3], labels[6], Float32[0., 0., 1.])

      T = typeof(labels[1])
      labels2[k] = (k in 1:3 ? randn(T)*0.25f0
                             : randn(T)*π/4.f0)
      draw!(img2, labels2[1], labels2[4], Float32[1., 0., 0.])
      draw!(img2, labels2[2], labels2[5], Float32[0., 1., 0.])
      draw!(img2, labels2[3], labels2[6], Float32[0., 0., 1.])
      (img, img2)
    end

    function traintensor(::Type{T}, N; noise=0.1, rand_seed=RAND_SEED_TRAIN) where T
        # we generate random y-offsets in [-0.5, 0.5] for three lines
        # and three angles in [-pi/2, pi/2]
        train_labels = trainlabels(T, N, rand_seed=rand_seed)
        train_tensors = [
           draw_labels_on_image_two_ways!(rand(T, 28, 28, 3)*0.1f0, y, k)
           for (y, k) in train_labels
        ]
        return train_tensors
    end

    function testtensor(::Type{T}, N; noise=0.1, rand_seed=RAND_SEED_TEST) where T
        traintensor(T, N; noise=noise, rand_seed=RAND_SEED_TEST)
    end

    function trainlabels(::Type{T}, N; rand_seed=RAND_SEED_TRAIN) where T
        Random.seed!(rand_seed)
        # we generate random y-offsets in [-0.5, 0.5] for three lines
        # and three angles in [-pi/2, pi/2]
        train_labels = [
            (vcat(randn(T, 3)*0.25f0, randn(T, 3) * π/4.f0), rand(1:6))
            for _ in 1:N]
        train_labels
    end

    function testlabels(::Type{T}, N; rand_seed=RAND_SEED_TEST) where T
        trainlabels(T, N; rand_seed=rand_seed)
    end

    function traindata(::Type{T}, args...) where T
        (traintensor(T, args...),
        trainlabels(T, args...))
    end

    function testdata(::Type{T}, args...) where T
        (testtensor(T, args...),
        testlabels(T, args...))
    end
end
