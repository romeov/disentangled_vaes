using FileIO

function visualize_test_images(model, test_images, epoch; basepath="/tmp")
  H, W = size(test_images)[1:2]
  model_out = model(test_images)
  if model_out isa Tuple
    test_reconstructed = model_out[1] .|> σ |> cpu
  else
    test_reconstructed = model_out .|> σ |> cpu
  end
  test_images = test_images |> cpu

  # stack images and line them up next to each other
  out_lhs = permutedims(test_images, (1, 4, 2, 3)) |> x -> reshape(x, :, W, 3)
  out_rhs = permutedims(test_reconstructed, (1, 4, 2, 3)) |> x -> reshape(x, :, W, 3)
  out = hcat(out_lhs, out_rhs)
  out = hcat(
             (out[H*8*(i-1)+1:H*8*i, :, :] for i in 1:4)...
            )
  save(joinpath(basepath, "vis$(epoch).png"), out)
end
