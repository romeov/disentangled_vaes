import Pkg
using Setfield, Random, Distributions
using HDF5: h5read
onfirst(f) = ((lhs, rhs...)::Tuple) -> (f(lhs), rhs...)

transform = begin
  USED_LATENT_DIM = 10  # TODO Fix this
  function insert_dim(A, i)
    sz = [size(A)...];
    insert!(sz, i, 1)
    reshape(A, sz...)
  end
  insert_dim(i :: Integer) = Base.Fix2(insert_dim, i)

  data = begin
      paths = [joinpath("data", "disentangled_data_$(loc).h5")
              for loc in ["taxiway", "kmwh"]]
      data_sets = [h5read(p, "color_imgs") for p in paths]
      cat([d |> insert_dim(4) for d in data_sets]..., dims=4)
  end;

  P = 0.5  # content or style
  H = W = 64
  I_img = CartesianIndices((3, H, W))
  I_s = CartesianIndices((2, 5, 15))
  L_s = length(I_s.indices)  # latent dim of style
  I_c = CartesianIndices((15, 15))
  L_c = length(I_c.indices)  # latent dim of content
  function make_sample()
      perm(x) = permutedims(x, [2, 3, 1])
      if rand() < P  # change "content"
          i = sample(1:L_c)  # which latent to change
          idx_c = sample(I_c)
          idx_c_lhs = @set (idx_c[i] = sample(I_c.indices[i]))
          idx_c_rhs = @set (idx_c[i] = sample(I_c.indices[i]))
          idx_s = sample(I_s)
          x_lhs = data[I_img, idx_s, idx_c_lhs] ./ 255 .|> Float32 |> perm
          x_rhs = data[I_img, idx_s, idx_c_rhs] ./ 255 .|> Float32 |> perm
          ks = [i]
      else  # change "style"
          i = sample(1:L_s)  # which latent to change
          idx_c = sample(I_c)  # fix a content
          idx_s_lhs = sample(I_s)
          idx_s_rhs = sample(I_s)
          x_lhs = data[I_img, idx_s_lhs, idx_c] ./ 255 .|> Float32 |> perm
          x_rhs = data[I_img, idx_s_rhs, idx_c] ./ 255 .|> Float32 |> perm
          ks = L_c .+ (1:(USED_LATENT_DIM-L_c))
      end
      return (x_lhs, x_rhs), ks
  end

  function make_batch((ind, _)) :: Batch_t
      N = length(ind)
      samples = [make_sample() for _ in 1:N]
      x_lhs = cat([s[1][1] for s in samples]..., dims=4)
      x_rhs = cat([s[1][2] for s in samples]..., dims=4)
      ks = [s[2] for s in samples]
      return (x_lhs, x_rhs), ks
  end
end

# Implementation details:
# Note that the device_ch is unbuffered.
# Thus, the spawned thread loads data X to gpu and then waits at the
# `put!` command until the previously batch is consumed by the main thread.
"Asynchronously apply data transform to batches and bring one batch to the device."
async_data_prep(train_loader, transform_fn, device; num_threads=4) =
    Channel{Batch_t}(spawn=true) do device_ch
      transformed_data = throttled_parallel_data_transform(
          train_loader, transform_fn, num_threads)
      for (X, Y) in transformed_data
        X = X |> device
        put!(device_ch, (X, Y))
      end
    end


# Implementation details:
# We don't want to transform all the batches at once.
# Therefore, we use a "throttle_channel" that only let's though
# a fixed number of workers.
"Apply data transform on multiple threads."
throttled_parallel_data_transform(train_loader, transform_fn, num_threads) =
    Channel{Batch_t}(num_threads; spawn=true) do threaded_ch
      throttle_channel = Channel{Tuple}(num_threads)  # make sure only N threads are running at a time
      @sync for (x, y) in train_loader
        put!(throttle_channel, ())  # "take a spot"
        Threads.@spawn begin
          put!(threaded_ch, transform((x, y)))
          take!(throttle_channel)  # "release a spot" when finished
        end
      end
    end
