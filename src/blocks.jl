using Flux, CUDA, Zygote
include("./type_aliases.jl")

struct ResidualBlock
  conv_1 :: Conv
  conv_2 :: Conv
  λ :: AbstractVector{Float32}  # this only has one element, but Flux doesn't support scalars
end
ResidualBlock(channels::Int) = ResidualBlock(
                Conv((3, 3), channels=>channels, identity; pad=SamePad()),
                Conv((3, 3), channels=>channels, identity; pad=SamePad()),
                rand(Float32, 1)*1f-2,
               )
Flux.@functor ResidualBlock
function (b::ResidualBlock)(x :: Arr4) :: Arr4
  z = leakyrelu.(x)
  z = b.conv_1(z)
  z = leakyrelu.(z)
  z = b.conv_2(z)
  z = b.λ .* z
  return x+z
end


"Multiple outputs, see https://fluxml.ai/Flux.jl/stable/models/advanced/#Multiple-outputs:-a-custom-Split-layer"
struct Split{T}
  paths::T
end
Split(paths...) = Split(paths)
Flux.@functor Split
(m::Split)(x :: AbstractArray) = map(f -> f(x), m.paths)


"Adaptive Instance normalization.
Proposed somewhere else, but taken from the StyleGAN paper, Eq. (1).
https://arxiv.org/pdf/1812.04948.pdf"
struct AdaIN
  A
  norm :: InstanceNorm
end
AdaIN(latent_dim::Int, channels::Int) = AdaIN(
                Split(Chain(Dense(latent_dim, channels), x->reshape(x, 1, 1, channels, :)),
                      Chain(Dense(latent_dim, channels), x->reshape(x, 1, 1, channels, :))),
                InstanceNorm(channels),
)
Flux.@functor AdaIN
function (ada::AdaIN)(x :: Arr4, z :: Arr2) :: Arr4
  y_s, y_b = ada.A(z)
  return ada.norm(x) .* (y_s .+ 1.f0) .+ y_b
end
Flux.trainable(ada::AdaIN) = (ada.A, )  # we don't want to train the affine parameters


struct StyleGANBlockInit
  C :: AbstractArray{Float32, 4}
  B_1 :: AbstractArray{Float32, 4}
  ada_1 :: AdaIN
  conv_2 :: Conv
  B_2 :: AbstractArray{Float32, 4}
  ada_2 :: AdaIN
  inject_noise :: Bool
end
StyleGANBlockInit(init_dim::Int, latent_dim::Int, channels_in_out::Pair{Int, Int}; pad=SamePad(), inject_noise::Bool=false) = begin
  c_in, c_out = channels_in_out
  StyleGANBlockInit(
                rand(Float32, init_dim, init_dim, c_in, 1),
                ones(Float32, 1, 1, c_out, 1),
                AdaIN(latent_dim, c_in),
                Conv((3, 3), c_in=>c_out, identity; stride=1, pad=pad),
                ones(Float32, 1, 1, c_out, 1),
                AdaIN(latent_dim, c_out),
                inject_noise,
               )
end
Flux.@functor StyleGANBlockInit
function (b::StyleGANBlockInit)(z :: Arr2) :: Arr4
  M = z isa CuArray ? CUDA : Base
  B = size(z)[end]
  repmat :: Arr4 = Zygote.@ignore begin
    M.ones(Float32, 1, 1, 1, B)  
  end
  x = b.C .* repmat
  if b.inject_noise
    noise = Zygote.@ignore begin
      M.randn(Float32, size(x)[1], size(x)[2], 1, B)
    end
    x += b.B_1 .* noise
  end
  x = b.ada_1(x, z)
  x = b.conv_2(x)
  if b.inject_noise
    noise = Zygote.@ignore begin
      M.randn(Float32, size(x)[1], size(x)[2], 1, B)
    end
    x += b.B_2 .* noise
  end
  x = leakyrelu.(x)
  x = b.ada_2(x, z)
  @assert size(x)[end] == size(z)[end]
  return x
end


struct StyleGANBlock
  up :: Upsample
  conv_1x1 :: Conv
  conv_1 :: Conv
  B_1 :: AbstractArray{Float32, 4}
  ada_1 :: AdaIN
  conv_2 :: Conv
  B_2 :: AbstractArray{Float32, 4}
  ada_2 :: AdaIN
  inject_noise :: Bool
  use_residual_connection :: Bool
end
StyleGANBlock(latent_dim::Int, channels_in_out::Pair{Int, Int}; pad=SamePad(), inject_noise=false, use_residual_connection=true) = begin
  c_in, c_out = channels_in_out
  StyleGANBlock(
                Upsample(scale=2),
                Conv((1, 1), c_in=>c_out, leakyrelu; stride=1),
                Conv((3, 3), c_out=>c_out, leakyrelu; stride=1, pad=pad),
                ones(Float32, 1, 1, c_out, 1),
                AdaIN(latent_dim, c_out),
                Conv((3, 3), c_out=>c_out, leakyrelu; stride=1, pad=pad),
                ones(Float32, 1, 1, c_out, 1),
                AdaIN(latent_dim, c_out),
                inject_noise,
                use_residual_connection,
               )
end
Flux.@functor StyleGANBlock
function (b::StyleGANBlock)(x :: Arr4, z :: Arr2) :: Arr4
  M = z isa CuArray ? CUDA : Base
  B = size(z)[end]
  x = b.up(x)
  x = b.conv_1x1(x)
  x_id = x  # for residual connection
  x = b.conv_1(x)
  if b.inject_noise
    noise = Zygote.@ignore begin
      M.randn(Float32, size(x)[1], size(x)[2], 1, B)
    end
    x = x .+ b.B_1 .* typeof(x)()
  end
  x = leakyrelu.(x)
  x = b.ada_1(x, z)
  x = b.conv_2(x)
  if b.inject_noise
    noise = Zygote.@ignore begin
      M.randn(Float32, size(x)[1], size(x)[2], 1, B)
    end
    x = x .+ b.B_2 .* noise
  end
  x = leakyrelu.(x)
  x = b.ada_2(x, z)
  if b.use_residual_connection
    x += x_id
  end
  @assert size(x)[end] == size(z)[end]
  return x
end
