using Flux, CUDA
import Base.^
include("./type_aliases.jl")
include("./blocks.jl")
include("./compute_decoder_dims.jl")

struct AutoEncoder
  encoder
  decoder
end
AutoEncoder(target_dim; latent_dim=10) = AutoEncoder(Encoder(latent_dim), Decoder(latent_dim, target_dim))
Flux.@functor AutoEncoder
function (enc::AutoEncoder)(x)
  μ, _ = enc.encoder(x)
  x = enc.decoder(μ)
  return sigmoid.(x)
end

lookup_model(s::String) = Dict(
      "ResidualEncoder" => ResidualEncoder,
      "ResidualDecoder" => ResidualDecoder,
      "SAMEncoder"      => SAMEncoder,
      "StyleGANDecoder" => StyleGANDecoder,
      "BasicEncoder"    => BasicEncoder,
      "BasicDecoder"    => BasicDecoder,
  )[s]

struct VAE
  encoder
  decoder
end
VAE(image_dim :: Int, latent_dim :: Int; sc=1) = VAE(
    SAMEncoder(image_dim, latent_dim),
    StyleGANDecoder(latent_dim, image_dim))
make_vae(image_dim :: Int, latent_dim :: Int;
         encoder="ResidualEncoder", decoder="ResidualDecoder",
         sc=1) =
    VAE(
        lookup_model(encoder)(image_dim, latent_dim; sc=sc),
        lookup_model(decoder)(latent_dim, image_dim; sc=sc),
    )

Flux.@functor VAE
Flux.trainable(vae::VAE) = (vae.encoder, vae.decoder, )
function (vae::VAE)(x :: Arr4) :: Tuple{Arr4, Arr2, Arr2}
  B = size(x)[end]
  (μ :: Arr2, logvar :: Arr2) = vae.encoder(x)

  latent_dim = size(μ)[end-1]
  # There seems to be a problem with CUDA.randn and Zygote
  # randn_fn = (x isa CuArray ? CUDA.randn : Base.randn)
  # z = randn_fn(Float32, latent_dim, B) .* exp.(logσ) .+ μ
  noise :: Arr2 = Zygote.@ignore begin
    M = x isa CuArray ? CUDA : Base
    M.randn(Float32, latent_dim, B)
  end
  z :: Arr2 = noise .* exp.(logvar/2) .+ μ

  x = vae.decoder(z)
  return x, μ, logvar
end

(^)(f::Function, i::Int) = i==0 ? identity : f^(i-1)∘f
Base.div(i::Integer) = Base.Fix2(Base.div, i)
shrink(x) = (x-1)÷2
struct Encoder
  backbone :: Chain
  μ :: Union{Dense, Chain}
  logvar :: Union{Dense, Chain}
end
Flux.@functor Encoder
function (e::Encoder)(x :: Arr4) :: Tuple{Arr2, Arr2}
  activations :: Arr2 = e.backbone(x)
  μ :: Arr2 = e.μ(activations)
  logvar :: Arr2 = e.logvar(activations)
  return (μ, logvar)
end

BasicEncoder(input_dim, latent_dim; sc=1) = Encoder(
                Chain(
                      Conv((3, 3), 3=>32, relu; stride=2),
                      Conv((3, 3), 32=>64, relu; stride=2),
                      Conv((3, 3), 64=>128, relu; stride=2),
                      Flux.flatten,
                     ),
                Chain(Dense((shrink^3)(input_dim)^2*128, 128, relu), Dense(128, latent_dim)),
                Chain(Dense((shrink^3)(input_dim)^2*128, 128, relu), Dense(128, latent_dim)),
               )

ResidualBackbone(;sc = 1) = Chain(
                      Conv((5, 5), 3=>32÷sc, leakyrelu; stride=2, pad=SamePad()),
                      ResidualBlock(32÷sc),
                      ResidualBlock(32÷sc),
                      Conv((1, 1), 32÷sc=>64÷sc, identity; stride=1),

                      MeanPool((2, 2)),
                      ResidualBlock(64÷sc),
                      ResidualBlock(64÷sc),

                      MeanPool((2, 2)),
                      ResidualBlock(64÷sc),
                      ResidualBlock(64÷sc),
                      Conv((1, 1), 64÷sc=>128÷sc, identity; stride=1),
                )

FlattenHead(input_dim; sc=1) = Chain(
                      MeanPool((2, 2)),
                      Flux.flatten,
                      Dense((input_dim÷2÷2÷2÷2)^2*128÷sc, 128÷sc, leakyrelu),
                      LayerNorm(128÷sc),
                     )

ResidualEncoder(input_dim, latent_dim; sc=1) = Encoder(
                Chain(
                  ResidualBackbone(),
                  FlattenHead(input_dim),
                ),
                # Special initialization, see https://arxiv.org/pdf/2010.14407.pdf, Table 2 (Appendix)
                Dense(0.1f0*Flux.glorot_uniform(latent_dim, 128÷sc), -1*ones(Float32, latent_dim)),  # μ
                Dense(0.1f0*Flux.glorot_uniform(latent_dim, 128÷sc), -1*ones(Float32, latent_dim)),  # logvar
               )

SAMEncoder(input_dim, latent_dim; sc=1) = Encoder(
                Chain(
                  ResidualBackbone(),
                  SoftArgMaxHead(input_dim÷(2^3), input_dim÷(2^3)),
                ),
                Chain(
                      Dense(3*128÷sc, 512÷sc, leakyrelu),
                      Dense(0.1f0*Flux.glorot_uniform(latent_dim, 512÷sc), -1*ones(Float32, latent_dim)),  # μ
                ),
                Chain(
                      Dense(3*128÷sc, 512÷sc, leakyrelu),
                      Dense(0.1f0*Flux.glorot_uniform(latent_dim, 512÷sc), -1*ones(Float32, latent_dim)),  # logvar
                ),
)


struct SoftArgMaxHead
  R_x  :: Arr4
  R_y  :: Arr4
end
SoftArgMaxHead(W::Integer, H::Integer) = SoftArgMaxHead(
    [ i*1f0/ W - 1f0/(2*W) for i in 1:W, j in 1:H, c in 1:1, b in 1:1],
    [ j*1f0/ W - 1f0/(2*H) for i in 1:W, j in 1:H, c in 1:1, b in 1:1],
)
Flux.@functor SoftArgMaxHead
"Computes xy position of maximum activation."
function (l::SoftArgMaxHead)(X :: Arr4) :: Arr2
  max_activations = GlobalMaxPool()(X)[1, 1, :, :]
  X_ = softmax(X, dims=[1, 2]);
  xs = sum(X_ .* l.R_x, dims=[1, 2])[1, 1, :, :]
  ys = sum(X_ .* l.R_y, dims=[1, 2])[1, 1, :, :]
  z = vcat(xs, ys, max_activations)
end


"See ON THE TRANSFER OF DISENTANGLED REPRESENTATIONS IN REALISTIC SETTINGS, Appendix A
 https://arxiv.org/pdf/2010.14407.pdf"
ResidualDecoder(latent_dim, output_dim; sc=1) = Chain(
      Dense(latent_dim, 128÷sc, leakyrelu),
      Dense(128÷sc, 4^2*128÷sc, identity),  # identity here because we have relu in ResidualBlock
      x->reshape(x, 4, 4, 128÷sc, :),
      ResidualBlock(128÷sc),
      ResidualBlock(128÷sc),

      Upsample(2, :bilinear),
      ResidualBlock(128÷sc),
      ResidualBlock(128÷sc),
      Conv((1, 1), 128÷sc=>64÷sc, identity; stride=1),

      Upsample(2, :bilinear),
      ResidualBlock(64÷sc),
      ResidualBlock(64÷sc),

      Upsample(2, :bilinear),
      ResidualBlock(64÷sc),
      ResidualBlock(64÷sc),
      Conv((1, 1), 64÷sc=>32÷sc, identity; stride=1),

      Upsample(2, :bilinear),
      x->leakyrelu.(x),
      Conv((5, 5), 32÷sc=>3, identity; stride=1, pad=SamePad()),
)

BasicDecoder(latent_dim, output_dim; sc=1) = Chain(Dense(latent_dim, 256, leakyrelu),
      Dense(256, 1024, leakyrelu),
      x->reshape(x, 4, 4, 64, :),
      Conv((3, 3), 64=>32, leakyrelu; stride=1, pad=SamePad()),
      Conv((1, 1), 32=>32, identity; stride=1),
      Upsample(2, :bilinear),
      Conv((3, 3), 32=>16, leakyrelu; stride=1, pad=SamePad()),
      Conv((1, 1), 16=>16, identity; stride=1),
      Upsample(2, :bilinear),
      Conv((3, 3), 16=>8, leakyrelu; stride=1),
      Upsample(2, :bilinear),
      Conv((1, 1), 8=>3, identity; stride=1),
)

"See StyleGAN paper, Fig. 1 (b).
https://arxiv.org/pdf/1812.04948.pdf"
struct StyleGANDecoder
  latent_enc :: Union{Dense, Chain}
  blk1 :: StyleGANBlockInit
  blk2 :: StyleGANBlock
  blk3 :: StyleGANBlock
  blk4 :: StyleGANBlock
  conv1x1 :: Conv
end
StyleGANDecoder(latent_dim, target_dim; sc=1) = begin
  init_dim, Δ = compute_decoder_dims(target_dim) |> values
  style_dim = 256
  f(Δ) = (Δ == 0 ? SamePad() : 0)  # (Δ==0): don't reduce size; (Δ==1): reduce size
  StyleGANDecoder(
          Chain(Dense(latent_dim, style_dim, leakyrelu),
                Dense(style_dim, style_dim, leakyrelu)),
          StyleGANBlockInit(init_dim, style_dim, 128=>96; pad=f(Δ[1])),
          StyleGANBlock(style_dim, 96=>64; pad=f(Δ[2])),
          StyleGANBlock(style_dim, 64=>32; pad=f(Δ[3])),
          StyleGANBlock(style_dim, 32=>16; pad=f(Δ[4])),  # pad=0 so that we get 28x28, otherwise we'd get 32x32
          Conv((1, 1), 16=>3, identity),
  )
end
Flux.@functor StyleGANDecoder
function (d::StyleGANDecoder)(z :: Arr2) :: Arr4
  z = d.latent_enc(z)
  x = d.blk1(z)
  x = d.blk2(x, z)
  x = d.blk3(x, z)
  x = d.blk4(x, z)
  x = d.conv1x1(x)  # now 28x28
  return x
end
