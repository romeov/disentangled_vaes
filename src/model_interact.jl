using BSON: @load
using Flux
using GLMakie
using Images
using BSON: @load, load
include("./data_utils.jl")
include("./models.jl")

function interactive(model_path :: String)
  model = load(model_path)[:enc_cpu]
  interactive(model)
end

function interactive(model)
  test_images = (1:32, 1:32) |> transform |> first |> first
  H, W = size(test_images)[1:2]

  imgview(x) = colorview(RGB, permutedims(x[:, end:-1:1, :, 1], (3, 1, 2)) .|> Float64)

  rnd_idx = Observable(rand(1:size(test_images)[end]))
  img = lift(rnd_idx) do rnd_idx
    img = test_images[:, :, :, rnd_idx:rnd_idx]
    img
  end

  latent_dim = begin
    _, μ, _ = model(img[])
    latent_dim=size(μ)[1]
  end
  latent_dim_ = min(latent_dim, 10)

  fig = Figure()
  ax1 = Makie.Axis(fig[1, 1]; aspect=DataAspect(), title="Original image")
  img_plottable = lift(img) do img
      img |> cpu |> imgview
  end
  image!(ax1, img_plottable; interpolate=false)

  slider_widget = labelslidergrid!(fig,
                            ["z_i+$j" for j in 1:latent_dim_],
                            [-2:5f-2:2 for _ in 1:latent_dim_];
                            formats=[x->"$(round(x, digits=2))" for _ in 1:latent_dim_],
                            snap=true,
                            )
  for s in slider_widget.sliders
    set_close_to!(s, 0.0)
  end
  fig[2, 1] = slider_layout = GridLayout()
  slider_layout[1, 1] = slider_widget.layout
  slider_vals = [s.value for s in slider_widget.sliders]
  slider_layout[1, 2] = sigma_layout = GridLayout()

  button_grid = GridLayout()
  fig[2, 2] = button_grid
  sl_offset = labelslidergrid!(fig, ["i", ], [0:max(0, latent_dim-10), ], startvalue = 0, tellwidth=false)
  button_grid[4, 1] = sl_offset.layout

  μ = lift(img) do img
      model(img)[2]
  end
  logvar = lift(img) do img
      model(img)[3]
  end
  sigma_texts = [Label(sigma_layout[i, 1], lift(lgv->"σ=$(exp(lgv[i]/2) |> x->round(x; digits=2))", logvar)) for i in 1:latent_dim_]
  img_constructed = lift(μ, logvar, sl_offset.sliders[1].value, slider_vals...) do μ, logvar, offset, λs...
    λ = typeof(μ)(zeros(Float32, size(μ)))
    for (i, el) in enumerate(λs)
      λ[i+offset, 1] = el
    end
    # λ = typeof(μ)([λs... ;; ]')
    # λ = vcat(λ, typeof(μ)(ones(Float32, size(μ)[1]-latent_dim, 1)))
    lat = randn(Float32, size(μ)) .* exp.(logvar/2) .+ μ .+ λ
    σ.(model.decoder(lat)) |> imgview |> cpu
  end
  ax2 = Makie.Axis(fig[1, 2]; aspect=DataAspect(), title="Reconstructed image")
  image!(ax2, img_constructed; interpolate=false)

  reset_button = Button(button_grid[1, 1]; label="Reset", tellwidth = false)
  on(reset_button.clicks) do _
    for s in slider_widget.sliders
      set_close_to!(s, 0.0)
    end
  end
  resample_button = Button(button_grid[2, 1]; label="Resample", tellwidth = false)
  on(resample_button.clicks) do _
    s = slider_widget.sliders[1]
    set_close_to!(s, s.value[])
  end
  new_image_button = Button(button_grid[3, 1]; label="New image", tellwidth = false)
  on(new_image_button.clicks) do _
      rnd_idx[] = rand(1:size(test_images)[end])
  end

  fig
end
