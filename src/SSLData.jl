export SSLData

module SSLData
    using HDF5
    using Random

    export traintensor, testtensor, trainlabels, testlabels, traindata, testdata
    # export full_data, train_ind_cross, train_ind_angle, test_ind_cross, test_ind_angle

    const RAND_SEED = 42
    Random.seed!(RAND_SEED)
    const SPLIT=0.8
    const full_data = h5open("data/disentangled_data.h5", "r")["color_imgs"][]
    const train_ind_cross = randperm(size(full_data)[4]) |> x->first(x, round(Int, length(x)*SPLIT))
    const train_ind_angle = randperm(size(full_data)[5]) |> x->first(x, round(Int, length(x)*SPLIT))
    const test_ind_cross  = randperm(size(full_data)[4]) |> x-> last(x, round(Int, length(x)*(1-SPLIT)))
    const test_ind_angle  = randperm(size(full_data)[5]) |> x-> last(x, round(Int, length(x)*(1-SPLIT)))


    function traintensor(::Type{T}, args...; dir = nothing) where T
        return repeat(vcat(train_ind_cross, train_ind_angle), 20)

    end

    function testtensor(::Type{T}, args...; dir = nothing) where T
        return repeat(vcat(test_ind_cross, test_ind_angle), 20)
    end

    function trainlabels(args...; dir = nothing)
        return repeat(vcat(ones(Int, length(train_ind_cross)), 2*ones(Int, length(train_ind_angle))), 20)
    end

    function testlabels(args...; dir = nothing)
        return repeat(vcat(ones(Int, length(test_ind_cross)), 2*ones(Int, length(test_ind_angle))), 20)
    end

    function traindata(::Type{T}, args...; dir = nothing) where T
        (traintensor(T, args...; dir = dir),
        trainlabels(args...; dir = dir))
    end

    traindata(args...; dir = nothing) = traindata(N0f8, args...; dir = dir)

    function testdata(::Type{T}, args...; dir = nothing) where T
        (testtensor(T, args...; dir = dir),
        testlabels(args...; dir = dir))
    end

    testdata(args...; dir = nothing) = testdata(N0f8, args...; dir = dir)

end
