using Zygote
using Flux
using MLDatasets: MNIST
using CUDA
using ProgressMeter: Progress, next!
using Distributions: Bernoulli, logpdf
using StatsBase: mean
using BSON: @save
using DataFrames: DataFrame, push!
using CSV
import TOML
using ArgParse
using TensorBoardLogger, Logging

function make_training_args_parser()
    train_arg_parser = ArgParseSettings()
    @add_arg_table! train_arg_parser begin
        "--epochs"
        arg_type = Int
        default = 3_200
        "--lr"
        arg_type = Float64
        default = 1e-4
        "--latent_dim"
        arg_type = Int
        default = 10
        "--reg"
        arg_type = Float64
        default = 1e-4
        "--beta"
        arg_type = Float64
        default = 1e0
        "--batchsize"
        arg_type = Int
        default = 32
        "--kl_warmup"
        help = "Number of steps to ramp up KL loss."
        arg_type = Int
        default = 10_000
        "--lr_warmup"
        help = "Number of steps to ramp up learning rate."
        arg_type = Int
        default = 0
        "--num_threads"
        arg_type = Int
        default = 8
        "--encoder"
        arg_type = String
        default = "SAMEncoder"
        range_tester = x->x in ["SAMEncoder", "ResidualEncoder", "BasicEncoder"]
        "--decoder"
        arg_type = String
        default = "ResidualDecoder"
        range_tester = x->x in ["StyleGANDecoder", "ResidualDecoder", "BasicDecoder"]
        "--sc"
        help = "Model scaling factor. sc > 1 <=> model smaller."
        arg_type = Int
        default = 1
        "--output_dir"
        help = "Directory where output get's stored. Typically a combination of branch, diff and version."
        arg_type = String
        default = ""
    end
    train_arg_parser
end

### Some utilities
loss_mse(x, _) = Flux.Losses.mse(model(x), x)
gaussian_kl(μ, logvar; λ=1.) = 0.5 * sum( 1 ./λ .* (μ.^2 .+ exp.(logvar)) .- logvar .- 1 .+ log.(λ); dims=1 ) |> Flux.flatten
bernoulli_loss(x, x_rec) = Flux.Losses.logitbinarycrossentropy(x_rec, x; agg=x->sum(x; dims=[1,2,3])) |> Flux.flatten

weights(model) = [m.weight for m in Flux.modules(model) if ((m isa Dense) || (m isa Conv))]
reg_l2(params) = sum(x->sum(x.^2), params)

"Counts params in Dense and Conv layers"
approximate_model_params(model) = [sum(length, Flux.params(m)) for m in Flux.modules(model) if ((m isa Dense) || (m isa Conv))] |> sum
showmodel(model) = [m for m in Flux.modules(model) if ((m isa Dense) || (m isa Conv))]

"Main training function."
function run_training(args :: Dict)
  model_device = gpu
  experiment_path = (args["output_dir"] == "") ? make_experiment_path() : args["output_dir"]
  @info experiment_path
  open(joinpath(experiment_path, "args.toml"), "w") do io
    TOML.print(io, args)
  end

  train_loader = Flux.Data.DataLoader((1:3200, zeros(3200)), shuffle=false, batchsize=args["batchsize"], partial=false)
  test_loader = Flux.Data.DataLoader((1:320, zeros(320)), shuffle=true, batchsize=args["batchsize"], partial=false)
  data_for_vis = (1:32, 1:32) |> transform |> first |> first |> model_device

  model = make_vae(size(data_for_vis)[1], args["latent_dim"];
              encoder=args["encoder"], decoder=args["decoder"],
              sc=args["sc"]
          ) |> model_device

  showmodel(model)
  @info "Model has approx $(approximate_model_params(model)) params"

  "VAE loss.
  Reference: https://github.com/google-research/disentanglement_lib/blob/86a644d4ed35c771560dc3360756363d35477357/disentanglement_lib/methods/unsupervised/vae.py#L51"
  loss_vae(x::Arr4, ys::Int11; step=Inf) = begin
    x_rec, μ, logvar = model(x)
    mean( bernoulli_loss(x, x_rec) + args["beta"]*min(step/args["kl_warmup"], 1)*gaussian_kl(μ, logvar) )
  end
  loss_vae2((x_lhs, x_rhs)::Tuple{Arr4, Arr4}, ks::Int11; step=Inf) :: Real = begin
    μ_lhs, logvar_lhs = model.encoder(x_lhs)
    μ_rhs, logvar_rhs = model.encoder(x_rhs)

    latent_dim, B = size(μ_lhs)

    #= Construct S and \bar{S}, see Locatello 2020, Weakly-Supervised Disentanglement Without Compromises, Eq. 4-6
    = Recall: S masks the shared features, \bar{S} masks the non-shared features, i.e.
    =         S = [1 1 1 1 0 0 0 0 1 1 1 1 1 1 1 1] <=> z_1, z_3, and z_4 are shared.
    =#
    (S :: Arr2, S_bar :: Arr2) = Zygote.@ignore begin
      S = ones(Float32, latent_dim, B)
      for (i, k) in enumerate(ks) S[k, i] .= 0. end

      S = S |> model_device
      S_bar = (1.f0 .- S)
      # @assert all(sum(S_bar, dims=1) .== l_4) (sum(S_bar, dims=1), l_4)

      S, S_bar
    end

    μ_tilde_lhs = 1.f0/2 * S .* (μ_lhs .+ μ_rhs) + S_bar .* μ_lhs
    μ_tilde_rhs = 1.f0/2 * S .* (μ_lhs .+ μ_rhs) + S_bar .* μ_rhs

    logvar_tilde_lhs = 1.f0/2 * S .* (logvar_lhs .+ logvar_rhs) + S_bar .* logvar_lhs  # Note that var = exp(1/2(logvar1 + logvar2))
    logvar_tilde_rhs = 1.f0/2 * S .* (logvar_lhs .+ logvar_rhs) + S_bar .* logvar_rhs  #               = sqrt(logvar2) * sqrt(logvar2), which is reasonable

    M = x_lhs isa CuArray ? CUDA : Base
    noise_lhs, noise_rhs = Zygote.@ignore begin
      M.randn(Float32, latent_dim, B), M.randn(Float32, latent_dim, B)
    end
    z_lhs = noise_lhs .* exp.(logvar_tilde_lhs/2) .+ μ_tilde_lhs
    z_rhs = noise_rhs .* exp.(logvar_tilde_rhs/2) .+ μ_tilde_rhs

    x_rec_lhs = model.decoder(z_lhs)
    x_rec_rhs = model.decoder(z_rhs)

    λs = Zygote.@ignore vcat(M.ones(2), 10*M.ones(args["latent_dim"]-2))
    mean( bernoulli_loss(x_lhs, x_rec_lhs) + bernoulli_loss(x_rhs, x_rec_rhs)
         + args["beta"]*min(step/args["kl_warmup"], 1)
           * (  gaussian_kl(μ_tilde_lhs, logvar_tilde_lhs; λ=λs)
              + gaussian_kl(μ_tilde_rhs, logvar_tilde_rhs; λ=λs))
        )
  end


  loss(x::Tuple{Arr4, Arr4}, y::Int11; step=Inf) =
    loss_vae2(x, y; step=step) + args["reg"]*reg_l2(weights(model.decoder))

  params = Flux.params(model)
  opt = Flux.Optimiser(ClipNorm(1.0), ADAM(args["lr"]))

  log_df = DataFrame(:epoch=>[], :step=>[], :train_loss=>[], :bernoulli_loss=>[], :KL_loss=>[])
  tb_logger = occursin("/tmp", experiment_path) ? global_logger() : TBLogger(experiment_path, tb_overwrite)

  train_step = 1
  @info "Starting train!"
  for epoch in 1:args["epochs"]
    if args["lr_warmup"] > 0 && epoch <= args["lr_warmup"]
      opt.os[2].eta = args["lr"] * epoch / args["lr_warmup"]
    end

    progress = Progress(length(train_loader) + 1)  # one more for the test metric
    total_training_loss = 0.; total_train_samples = 0; total_train_batches = 0;
    total_bernoulli_loss = 0.; total_KL_loss = 0.
    total_test_loss = 0.; total_test_samples = 0

    # we transform the multiple batches in parallel
    # and asynchronously load one batch to the device
    prepared_data :: Channel{Batch_t} =
        async_data_prep(train_loader, transform, model_device;
                        num_threads=args["num_threads"])

    # Main training loop
    for (X, Y) in prepared_data
      # For debugging run once without gradient
      if (epoch == 1) && (total_train_samples == 0)
        loss(copy.(X), copy(Y); step=train_step)
      end

      grad = gradient(params) do
        training_loss = loss(X, Y; step=train_step)
        total_training_loss += training_loss; total_train_samples += size(X[1])[end]; total_train_batches += 1;
        return training_loss
      end
      bernoulli_loss_, KL_loss = Zygote.@ignore begin
        x_rec, μ, logvar = model(X[1])
        return mean(bernoulli_loss(X[1], x_rec)), mean(gaussian_kl(μ, logvar))
      end
      total_bernoulli_loss += bernoulli_loss_; total_KL_loss += KL_loss

      next!(progress; showvalues=[(:epoch, epoch), (:train_loss, total_training_loss/total_train_batches), (:bernoulli_loss, total_bernoulli_loss/total_train_batches), (:KL_loss, total_KL_loss/total_train_batches)])
      Flux.update!(opt, params, grad)
      train_step += 1
    end

    # Test loop
    for (X, Y) in test_loader
      (X, Y) = transform((X, Y))
      X = X |> model_device
      test_loss = loss_vae(X[1], Y)
      total_test_loss += test_loss; total_test_samples += size(X[1])[end]
    end

    # Logging
    next!(progress; showvalues=[(:epoch, epoch), (:train_loss, total_training_loss/total_train_samples), (:test_loss, total_test_loss/total_test_samples), (:bernoulli_loss, total_bernoulli_loss/total_train_samples), (:KL_loss, total_KL_loss/total_train_samples)])
    push!(log_df, Dict(:epoch=>epoch, :step=>train_step, :train_loss=>total_training_loss/total_train_samples, :bernoulli_loss=>total_bernoulli_loss/total_train_samples, :KL_loss=>total_KL_loss/total_train_samples))
    CSV.write(joinpath(experiment_path, "log.csv"), log_df)
    with_logger(tb_logger) do
      @info "train" epoch=epoch step=train_step train_loss=total_training_loss/total_train_samples bernoulli_loss=total_bernoulli_loss/total_train_samples KL_loss=total_KL_loss/total_train_samples log_step_increment=0
      @info "test" epoch=epoch step=train_step test_loss=total_test_loss/total_test_samples
    end

    # Save model
    enc_cpu = cpu(model)
    @save joinpath(experiment_path, "model$(epoch).bson") enc_cpu
    if (epoch-1)%10 != 0
      run(`rm $(joinpath(experiment_path, "model$(epoch-1).bson"))`; wait=false)  # don't care if it succeeds or not
    end

    visualize_test_images(model, data_for_vis, epoch; basepath=experiment_path)
  end
  @info "Finished train!"
  model, data_for_vis
end
