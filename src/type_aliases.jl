using CUDA: AnyCuArray
const Arr2 = Union{Array{Float32, 2}, AnyCuArray{Float32, 2}};
const Arr4 = Union{Array{Float32, 4}, AnyCuArray{Float32, 4}};
const Int11 = Vector{Vector{Integer}};
const Batch_t = Tuple{Tuple{Arr4, Arr4}, Vector{Vector{Integer}}};
