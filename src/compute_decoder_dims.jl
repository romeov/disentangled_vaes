using JuMP
using GLPK

"Solve a simple linear program to compute the padding we need to use.

Say we want the deconvolution to end up at 56x56.
We compute
```
56 = (((init_dim - 4*Δ_1) * 2 - 4*Δ_2) * 2 - 4*Δ_3) * 2 - 4*Δ4
   = 8*init_dim - 32*Δ_1 - 16*Δ_2 - 8*Δ_3 - 4*Δ_4
```
where each Δ_i decides whether to reduce the dimension
"
function compute_decoder_dims(target_dim :: Int)
  model = JuMP.Model(GLPK.Optimizer)
  @variable(model, init_dim >= 0, Int)
  @variable(model, Δ[i=1:4], Bin)
  @constraint(model, 2^3 * init_dim - 2 .* sum( 2 .^ (4:-1:1) .* Δ) == target_dim)
  @objective(model, Min, sum(Δ))
  optimize!(model)
  @assert termination_status(model) == MOI.OPTIMAL "Cannot find convolution padding to produce your wanted decoder output dimension: $(target_dim)"
  return Dict(
      :init_dim => Int(value(init_dim)),
      :Δ => Int.(value.(Δ))
  )
end
